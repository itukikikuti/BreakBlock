#include "XLibrary11.hpp"
using namespace XLibrary11;

int Main()
{
	const int width = 7;
	const int height = 3;

	Camera camera;
	camera.color = Float4(0.0f, 0.0f, 0.0f, 1.0f);

	Sprite blockSprite(L"block.png");
	blockSprite.scale.x = 2.0f;

	Float3 blockPositions[width][height];
	int blockLife[width][height];

	Sprite ballSprite(L"ball.png");
	Float3 ballSpeed;

	Sprite playerSprite(L"ball.png");
	playerSprite.position = Float3(0.0f, -200.0f, 0.0f);
	playerSprite.scale.x = 5.0f;

	Sound hitSound(L"hit.wav");
	Sound pointSound(L"point.wav");

	Window::SetSize(320, 480);

	bool initFlag = true;

	while (Refresh())
	{
		camera.Update();

		if (initFlag)
		{
			initFlag = false;

			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					Float3 offset(-width / 2 * 35.0f, 160.0f, 0.0f);
					blockPositions[x][y] = Float3(x * 35.0f, y * 20.0f, 0.0f) + offset;
					blockLife[x][y] = 3;
				}
			}

			ballSprite.position = Float3(0.0f, 0.0f, 0.0f);
			ballSpeed = Float3(0.0f, 5.0f, 0.0f);
		}

		initFlag = true;

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				if (blockLife[x][y] <= 0)
					continue;

				initFlag = false;

				blockSprite.position = blockPositions[x][y];

				Float3 now = ballSprite.position;
				Float3 next = ballSprite.position + ballSpeed;

				if (next.x > blockSprite.position.x - 24.0f &&
					next.x < blockSprite.position.x + 24.0f &&
					now.y > blockSprite.position.y - 16.0f &&
					now.y < blockSprite.position.y + 16.0f)
				{
					ballSpeed.x = -ballSpeed.x;
					blockLife[x][y]--;
					pointSound.Play();
				}

				if (now.x > blockSprite.position.x - 24.0f &&
					now.x < blockSprite.position.x + 24.0f &&
					next.y > blockSprite.position.y - 16.0f &&
					next.y < blockSprite.position.y + 16.0f)
				{
					ballSpeed.y = -ballSpeed.y;
					blockLife[x][y]--;
					pointSound.Play();
				}

                if (blockLife[x][y] == 3)
                {
                    blockSprite.color = Float4(0.0f, 1.0f, 0.0f, 1.0f);
                }
                else if (blockLife[x][y] == 2)
                {
                    blockSprite.color = Float4(1.0f, 1.0f, 0.0f, 1.0f);
                }
                else if (blockLife[x][y] == 1)
                {
                    blockSprite.color = Float4(1.0f, 0.0f, 0.0f, 1.0f);
                }

				blockSprite.Draw();
			}
		}

		ballSprite.position += ballSpeed;

		if (ballSprite.position.y > Window::GetSize().y / 2.0f)
		{
			ballSpeed.y = -fabsf(ballSpeed.y);
			hitSound.Play();
		}
		if (ballSprite.position.y < -Window::GetSize().y / 2.0f)
		{
			initFlag = true;
		}
		if (ballSprite.position.x > Window::GetSize().x / 2.0f)
		{
			ballSpeed.x = -fabsf(ballSpeed.x);
			hitSound.Play();
		}
		if (ballSprite.position.x < -Window::GetSize().x / 2.0f)
		{
			ballSpeed.x = fabsf(ballSpeed.x);
			hitSound.Play();
		}

		ballSprite.Draw();

		if (Input::GetKey(VK_RIGHT))
		{
			playerSprite.position.x += 5.0f;
		}
		if (Input::GetKey(VK_LEFT))
		{
			playerSprite.position.x -= 5.0f;
		}

		if (playerSprite.position.x > Window::GetSize().x / 2.0f)
		{
			playerSprite.position.x = Window::GetSize().x / 2.0f;
		}
		if (playerSprite.position.x < -Window::GetSize().x / 2.0f)
		{
			playerSprite.position.x = -Window::GetSize().x / 2.0f;
		}

		if (playerSprite.position.x - 48.0f < ballSprite.position.x &&
			playerSprite.position.x + 48.0f > ballSprite.position.x &&
			playerSprite.position.y - 16.0f < ballSprite.position.y &&
			playerSprite.position.y + 16.0f > ballSprite.position.y)
		{
			ballSpeed.x = (ballSprite.position.x - playerSprite.position.x) * 0.2f;
			ballSpeed.y = 5.0f;
			hitSound.Play();
		}

		playerSprite.Draw();
	}

	return 0;
}
